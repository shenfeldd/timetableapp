package com.shenfeld.timetableapp.ui.pack_bag

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.shenfeld.timetableapp.base.BaseViewModel
import com.shenfeld.timetableapp.extensions.Event
import com.shenfeld.timetableapp.extensions.asEvent
import com.shenfeld.timetableapp.models.CheckBoxState
import com.shenfeld.timetableapp.models.DayItem
import com.shenfeld.timetableapp.repositories.AppRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class PackBagViewModel(private val appRepository: AppRepository) : BaseViewModel() {

    private val _showLoaderLiveData = MutableLiveData(false)
    val showLoaderLiveData: LiveData<Boolean> = _showLoaderLiveData

    private val _commandLiveData = MutableLiveData<Event<Command>>()
    val commandLiveData: LiveData<Event<Command>> = _commandLiveData

    fun packBag(
        textbooksChecked: Boolean,
        pencilCaseChecked: Boolean,
        robeChecked: Boolean,
        flashDriveChecked: Boolean,
        sportsUniformsChecked: Boolean,
        dayItem: DayItem,
    ) = viewModelScope.launch {
        _showLoaderLiveData.value = true
        appRepository.set(
            name = dayItem.weekDay,
            newDayItem = DayItem(
                weekDay = dayItem.weekDay,
                listDisciplines = dayItem.listDisciplines,
                checkBoxesState = CheckBoxState(
                    isTextbooksChecked = textbooksChecked,
                    isPencilCaseChecked = pencilCaseChecked,
                    isRobeChecked = robeChecked,
                    isFlashDriveChecked = flashDriveChecked,
                    isSportsUniformsChecked = sportsUniformsChecked,
                )
            )
        )
        delay(1000L)
        _showLoaderLiveData.value = false
        _commandLiveData.value = Command.NavBack.asEvent()
    }

    sealed class Command {
        object NavBack : Command()
        object Nothing : Command()
    }
}