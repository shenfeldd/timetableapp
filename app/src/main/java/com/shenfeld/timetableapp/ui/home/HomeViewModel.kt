package com.shenfeld.timetableapp.ui.home

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.android.play.core.splitinstall.d
import com.shenfeld.timetableapp.R
import com.shenfeld.timetableapp.base.BaseViewModel
import com.shenfeld.timetableapp.enums.WeekDay
import com.shenfeld.timetableapp.extensions.Event
import com.shenfeld.timetableapp.extensions.asEvent
import com.shenfeld.timetableapp.models.CheckBoxState
import com.shenfeld.timetableapp.models.DayItem
import com.shenfeld.timetableapp.models.Discipline
import com.shenfeld.timetableapp.repositories.AppRepository
import kotlinx.coroutines.launch
import java.lang.StringBuilder
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.util.*

@SuppressLint("NewApi")
class HomeViewModel(private val appRepository: AppRepository) : BaseViewModel() {

    private val _daysStateLiveData = MutableLiveData<List<DayItem>>()
    val daysStateLiveData: LiveData<List<DayItem>> = _daysStateLiveData

    private val _commandLiveData = MutableLiveData<Event<Command>>()
    val commandLiveData: LiveData<Event<Command>> = _commandLiveData

    fun onViewCreated() = viewModelScope.launch {
        val daysState = appRepository.getRemindersMap()
        val dayStateList = daysState.map { it.value }
        _daysStateLiveData.value = dayStateList.sortedBy { it.weekDay.priority }
    }

    fun onPackBagClicked() = viewModelScope.launch {
        val nextDay = LocalDateTime.now().plusHours(24).dayOfWeek
        if (nextDay == DayOfWeek.SUNDAY) {
            _commandLiveData.value = Command.MakeToast(msg = R.string.home_fragment_pack_a_bag_toast).asEvent()
            return@launch
        }

        val dayToGet = WeekDay.getTypeByValue(
            nextDay.name.lowercase(Locale.getDefault())
                .replaceFirstChar {
                    if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString()
                })
        val dayItem = requireNotNull(appRepository.get(name = dayToGet))

        if (dayItem.listDisciplines.all { it == Discipline.UNKNOWN }) {
            _commandLiveData.value = Command.MakeToast(msg = R.string.home_fragment_pack_a_bag_toast_2, dayOfWeek = dayItem.weekDay).asEvent()
            return@launch
        }

        _commandLiveData.value = Command.NavForward(dayItem = dayItem).asEvent()
    }

    fun setInfoState() = viewModelScope.launch {
        val nextDay = LocalDateTime.now().plusHours(24).dayOfWeek
        val dayToGet = WeekDay.getTypeByValue(
            nextDay.name.lowercase(Locale.getDefault())
                .replaceFirstChar {
                    if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString()
                })
        val dayItem = requireNotNull(appRepository.get(name = dayToGet))
        if (checkCheckBoxes(checkBoxState = dayItem.checkBoxesState)) {
            _commandLiveData.value = Command.MakeToast(msg = R.string.home_fragment_things_for_tomorrow_not_exist).asEvent()
            return@launch
        }

        _commandLiveData.value = Command.SetInfoState(thingsForTomorrow = getStringForThings(dayItem.checkBoxesState)).asEvent()
    }

    private fun getStringForThings(checkBoxes: CheckBoxState): String {
        val infoString = StringBuilder()

        if (checkBoxes.isTextbooksChecked) infoString.append("- Textbooks\n")
        if (checkBoxes.isPencilCaseChecked) infoString.append("- Pencil case\n")
        if (checkBoxes.isRobeChecked) infoString.append("- Robe\n")
        if (checkBoxes.isFlashDriveChecked) infoString.append("- Flash drive\n")
        if (checkBoxes.isSportsUniformsChecked) infoString.append("- Sports uniforms\n")

        return infoString.toString()
    }

    private fun checkCheckBoxes(checkBoxState: CheckBoxState): Boolean {
        return checkBoxState.isTextbooksChecked.not() && checkBoxState.isPencilCaseChecked.not() && checkBoxState.isRobeChecked.not()
                && checkBoxState.isFlashDriveChecked.not() && checkBoxState.isSportsUniformsChecked.not()
    }

    sealed class Command {
        class NavForward(val dayItem: DayItem) : Command()
        class MakeToast(val msg: Int, val dayOfWeek: WeekDay? = null) : Command()
        class SetInfoState(val thingsForTomorrow: String) : Command()
        object Nothing : Command()
    }
}