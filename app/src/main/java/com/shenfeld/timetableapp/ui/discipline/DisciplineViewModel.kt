package com.shenfeld.timetableapp.ui.discipline

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.shenfeld.timetableapp.base.BaseViewModel
import com.shenfeld.timetableapp.extensions.Event
import com.shenfeld.timetableapp.extensions.asEvent
import com.shenfeld.timetableapp.models.DayItem
import com.shenfeld.timetableapp.models.Discipline
import com.shenfeld.timetableapp.repositories.AppRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DisciplineViewModel(private val appRepository: AppRepository) : BaseViewModel() {

    private val _showLoaderLiveData = MutableLiveData(false)
    val showLoaderLiveData: LiveData<Boolean> = _showLoaderLiveData

    private val _commandLiveData = MutableLiveData<Event<Command>>()
    val commandLiveData: LiveData<Event<Command>> = _commandLiveData

    fun setDayDisciplines(listDisciplines: List<Discipline>, dayItem: DayItem) = viewModelScope.launch {
        _showLoaderLiveData.value = true
        appRepository.set(
            name = dayItem.weekDay,
            newDayItem = DayItem(
                weekDay = dayItem.weekDay,
                listDisciplines = listDisciplines,
                checkBoxesState = dayItem.checkBoxesState
            )
        )
        delay(1000L)
        _showLoaderLiveData.value = false
        _commandLiveData.value = Command.NavBack.asEvent()
    }

    sealed class Command {
        object NavBack : Command()
        object Nothing : Command()
    }
}