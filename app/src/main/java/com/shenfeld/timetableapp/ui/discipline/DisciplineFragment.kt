package com.shenfeld.timetableapp.ui.discipline

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.google.android.play.core.internal.at
import com.shenfeld.timetableapp.R
import com.shenfeld.timetableapp.base.BaseFragment
import com.shenfeld.timetableapp.databinding.FragmentDisciplineBinding
import com.shenfeld.timetableapp.extensions.observeEvent
import com.shenfeld.timetableapp.models.Discipline
import kotlinx.android.synthetic.main.fragment_discipline.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DisciplineFragment : BaseFragment<DisciplineViewModel, FragmentDisciplineBinding>() {
    override val layoutId: Int = R.layout.fragment_discipline
    override val viewModel: DisciplineViewModel by viewModel()
    override fun getViewBinding(): FragmentDisciplineBinding =
        FragmentDisciplineBinding.inflate(layoutInflater)

    private val args: DisciplineFragmentArgs by navArgs()

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupClickListeners()
        setupObservers()
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setupView() {
        binding.tvTitleDay.text = args.dayItem.weekDay.title
        val arrAdapter = DisciplineAdapter(context!!)
        val arrOfSpinners = arrayListOf(
            binding.sFirstDiscipline,
            binding.sSecondDiscipline,
            binding.sThirdDiscipline,
            binding.sFourthDiscipline,
            binding.sFifthDiscipline,
            binding.sSixthDiscipline,
        )

        arrOfSpinners.forEach { spinner ->
            spinner.adapter = arrAdapter
            spinner.setSelection(1)
        }

        args.dayItem.listDisciplines.forEachIndexed { index, discipline ->
            arrOfSpinners[index].setSelection(discipline.id)
        }
    }

    private fun setupClickListeners() {
        binding.btnSetDisciplines.setOnClickListener {
            val listDisciplines = listOf(
                Discipline.getTypeByValue((sFirstDiscipline.selectedItem as Discipline).title),
                Discipline.getTypeByValue((sSecondDiscipline.selectedItem as Discipline).title),
                Discipline.getTypeByValue((sThirdDiscipline.selectedItem as Discipline).title),
                Discipline.getTypeByValue((sFourthDiscipline.selectedItem as Discipline).title),
                Discipline.getTypeByValue((sFifthDiscipline.selectedItem as Discipline).title),
                Discipline.getTypeByValue((sSixthDiscipline.selectedItem as Discipline).title),
            )

            if (listDisciplines.all { it == Discipline.UNKNOWN }) {
                Toast.makeText(context, R.string.discipline_fragment_set_disciplines_toast, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            viewModel.setDayDisciplines(listDisciplines, args.dayItem)
        }
    }

    private fun setupObservers() {
        viewModel.showLoaderLiveData.observe(viewLifecycleOwner) { needToShowLoader ->
            showLoader(needToShowLoader)
        }

        viewModel.commandLiveData.observeEvent(viewLifecycleOwner) { command ->
            when (command) {
                DisciplineViewModel.Command.NavBack -> navController.popBackStack()
                DisciplineViewModel.Command.Nothing -> {}
            }
        }
    }

    private fun showLoader(needToShow: Boolean) {
        with(binding) {
            btnSetDisciplines.isClickable = needToShow.not()
            vBackground.isVisible = needToShow
            pbLoader.isVisible = needToShow
        }
    }

}