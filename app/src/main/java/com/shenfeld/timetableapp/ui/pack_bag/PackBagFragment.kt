package com.shenfeld.timetableapp.ui.pack_bag

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.shenfeld.timetableapp.R
import com.shenfeld.timetableapp.base.BaseFragment
import com.shenfeld.timetableapp.databinding.FragmentPackBagBinding
import com.shenfeld.timetableapp.extensions.observeEvent
import com.shenfeld.timetableapp.models.Discipline
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.StringBuilder

class PackBagFragment : BaseFragment<PackBagViewModel, FragmentPackBagBinding>() {
    override val layoutId: Int = R.layout.fragment_pack_bag
    override val viewModel: PackBagViewModel by viewModel()
    override fun getViewBinding(): FragmentPackBagBinding = FragmentPackBagBinding.inflate(layoutInflater)
    private val args: PackBagFragmentArgs by navArgs()

    override fun onBackPressed(): Boolean {
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupObservers()
        setupListeners()
    }

    private fun setupView() {
        with(binding) {
            cbTextbooks.isChecked = args.dayItem.checkBoxesState.isTextbooksChecked
            cbPencilCase.isChecked = args.dayItem.checkBoxesState.isPencilCaseChecked
            cbRobe.isChecked = args.dayItem.checkBoxesState.isRobeChecked
            cbFlashDrive.isChecked = args.dayItem.checkBoxesState.isFlashDriveChecked
            cbSportsUniforms.isChecked = args.dayItem.checkBoxesState.isSportsUniformsChecked

            tvTomorrow.text = getString(R.string.pack_bag_schedule_for_tomorrow, args.dayItem.weekDay.title)
            tvSchedule.text = StringBuilder().apply {
                args.dayItem.listDisciplines.forEachIndexed { index, discipline ->
                    if (index < args.dayItem.listDisciplines.size - 1) {
                        append("${index + 1}. ${discipline.title}\n")
                    } else {
                        append("${index + 1}. ${discipline.title}")
                    }
                }
            }.toString()

            if (args.dayItem.listDisciplines.contains(Discipline.PHYSICAL_EDUCATION)) {
                cbSportsUniforms.isVisible = true
            }
        }
    }

    private fun setupObservers() {
        viewModel.showLoaderLiveData.observe(viewLifecycleOwner) { needToShowLoader ->
            showLoader(needToShowLoader)
        }

        viewModel.commandLiveData.observeEvent(viewLifecycleOwner) { command ->
            when (command) {
                PackBagViewModel.Command.NavBack -> navController.popBackStack()
                PackBagViewModel.Command.Nothing -> {}
            }
        }
    }

    private fun setupListeners() {
        binding.btnDone.setOnClickListener {
            viewModel.packBag(
                binding.cbTextbooks.isChecked,
                binding.cbPencilCase.isChecked,
                binding.cbRobe.isChecked,
                binding.cbFlashDrive.isChecked,
                binding.cbSportsUniforms.isChecked,
                args.dayItem
            )
        }
    }

    private fun showLoader(needToShow: Boolean) {
        with(binding) {
            btnDone.isClickable = needToShow.not()
            vBackground.isVisible = needToShow
            pbLoader.isVisible = needToShow
        }
    }
}