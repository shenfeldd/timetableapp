package com.shenfeld.timetableapp.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.timetableapp.base.BaseAdapter
import com.shenfeld.timetableapp.base.RvModelBase
import com.shenfeld.timetableapp.databinding.ItemDayBinding
import com.shenfeld.timetableapp.models.DayItem
import com.shenfeld.timetableapp.models.Discipline

class HomeAdapter(val onItemClicked: (DayItem) -> Unit) : BaseAdapter<DayItem, HomeAdapter.DayVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayVH {
        val inflater = LayoutInflater.from(parent.context)
        return DayVH(ItemDayBinding.inflate(inflater))
    }

    override fun onBindViewHolder(holder: DayVH, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class DayVH(private val binding: ItemDayBinding) : RecyclerView.ViewHolder(binding.root), Bindable<RvModelBase> {
        override fun bind(item: RvModelBase) {
            (item as DayItem).let { dayItem ->
                binding.tvDayOfWeek.text = dayItem.weekDay.title
                binding.tvDisciplines.text = getDescriptionForDisciplines(dayItem.listDisciplines)
                binding.cardView.setOnClickListener { onItemClicked.invoke(dayItem)  }
            }
        }
    }

    private fun getDescriptionForDisciplines(disciplines: List<Discipline>): String {
        if (disciplines.size == 1 && disciplines.first() == Discipline.UNKNOWN) return "Click to specify your disciplines"

        val resString = StringBuilder()
        disciplines.forEachIndexed { index, discipline ->
            if (index < disciplines.size - 1) {
                resString.append("${index + 1}. ${discipline.title}\n")
            } else {
                resString.append("${index + 1}. ${discipline.title}")
            }
        }

        return resString.toString()
    }
}