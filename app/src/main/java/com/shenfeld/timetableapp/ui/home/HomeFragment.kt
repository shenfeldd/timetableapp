package com.shenfeld.timetableapp.ui.home

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import com.shenfeld.timetableapp.R
import com.shenfeld.timetableapp.base.BaseFragment
import com.shenfeld.timetableapp.databinding.FragmentHomeBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.shenfeld.timetableapp.base.GridSpacingItemDecoration
import com.shenfeld.timetableapp.enums.WeekDay
import com.shenfeld.timetableapp.extensions.observeEvent
import com.shenfeld.timetableapp.ui.discipline.DisciplineFragment
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.util.*

class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>() {
    override val layoutId: Int = R.layout.fragment_home
    override val viewModel: HomeViewModel by viewModel()
    override fun getViewBinding(): FragmentHomeBinding = FragmentHomeBinding.inflate(layoutInflater)

    private lateinit var homeAdapter: HomeAdapter

    companion object {
        const val SPAN_SIZE_MAX = 2
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
        setupListeners()
        viewModel.onViewCreated()
    }

    private fun setupRecyclerView() {
        homeAdapter = HomeAdapter { dayItem ->
            navController.navigate(HomeFragmentDirections.actionHomeFragmentToDisciplineFragment(dayItem = dayItem))
        }
        val spanCount = 2
        val spacing = 50
        val includeEdge = false
        binding.rvWeekDays.apply {
            adapter = homeAdapter
            layoutManager = GridLayoutManager(context, SPAN_SIZE_MAX)
            addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))
        }
    }

    private fun setupObservers() {
        viewModel.daysStateLiveData.observe(viewLifecycleOwner) { currentDaysState ->
            homeAdapter.replaceAll(currentDaysState)
        }

        viewModel.commandLiveData.observeEvent(viewLifecycleOwner) { command ->
            when (command) {
                is HomeViewModel.Command.NavForward -> {
                    navController.navigate(
                        HomeFragmentDirections.actionHomeFragmentToPackBagFragment(
                            dayItem = command.dayItem
                        )
                    )
                }

                is HomeViewModel.Command.MakeToast -> {
                    Toast.makeText(context, getString(command.msg, command.dayOfWeek?.title ?: ""), Toast.LENGTH_SHORT).show()
                }

                is HomeViewModel.Command.SetInfoState -> {
                    binding.tvSchedule.text = command.thingsForTomorrow
                    binding.cvInfo.isVisible = true
                    binding.vBackground.isVisible = true
                }

                HomeViewModel.Command.Nothing -> {}
            }
        }
    }

    private fun setupListeners() {
        binding.btnPackABag.setOnClickListener {
            viewModel.onPackBagClicked()
        }

        binding.btnThingsForTomorrow.setOnClickListener {
            viewModel.setInfoState()
        }

        binding.btnOk.setOnClickListener {
            binding.cvInfo.isVisible = false
            binding.vBackground.isVisible = false
        }

        binding.vBackground.setOnClickListener {
            binding.cvInfo.isVisible = false
            binding.vBackground.isVisible = false
        }
    }
}