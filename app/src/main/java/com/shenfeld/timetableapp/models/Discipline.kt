package com.shenfeld.timetableapp.models

enum class Discipline(val title: String, val id: Int) {
    UNKNOWN("-", 1),
    GEOMETRY("Geometry", 2),
    ALGEBRA("Algebra", 3),
    ENGLISH("English", 4),
    PHYSICAL_EDUCATION("Physical education", 5),
    SOCIAL_STUDIES("Social studies", 6),
    GEOGRAPHY("Geography", 7),
    LITERATURE("Literature", 8),
    PHYSICS("Physics", 9),
    BIOLOGY("Biology", 10),
    MUSIC("Music", 11),
    FOREIGN_LANGUAGE("Foreign language", 12),
    ART("Art", 13),
    HISTORY("History", 14),
    INFORMATICS_AND_ICT("Informatics and ICT", 15),
    ASTRONOMY("Astronomy", 16),
    CHEMISTRY("Chemistry", 17),
    LIFE_SAFETY_BASICS("Life safety basics", 18);

    companion object {
        fun getTypeByValue(value: String): Discipline {
            return values().find { it.title.equals(value, ignoreCase = true) } ?: throw IllegalStateException("Check -> Disciplines enum class")
        }
    }
}