package com.shenfeld.timetableapp.models

import com.shenfeld.timetableapp.base.RvModelBase
import com.shenfeld.timetableapp.enums.WeekDay
import java.io.Serializable

data class DayItem(
    val weekDay: WeekDay,
    val listDisciplines: List<Discipline> = emptyList(),
    val checkBoxesState: CheckBoxState = CheckBoxState(),
) : RvModelBase(ItemType.DAY.ordinal), Serializable {
    enum class ItemType {
        DAY
    }
}