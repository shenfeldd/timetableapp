package com.shenfeld.timetableapp.models

data class CheckBoxState(
    val isTextbooksChecked: Boolean = false,
    val isPencilCaseChecked: Boolean = false,
    val isRobeChecked: Boolean = false,
    val isFlashDriveChecked: Boolean = false,
    val isSportsUniformsChecked: Boolean = false,
)