package com.shenfeld.timetableapp.enums

enum class WeekDay(val title: String, val priority: Int) {
    MONDAY("Monday", 1),
    TUESDAY("Tuesday", 2),
    WEDNESDAY("Wednesday", 3),
    THURSDAY("Thursday", 4),
    FRIDAY("Friday", 5),
    SATURDAY("Saturday", 6);

    companion object {
        fun getTypeByValue(value: String): WeekDay {
            return requireNotNull(values().find { it.title.equals(value, ignoreCase = true) })
        }
    }
}