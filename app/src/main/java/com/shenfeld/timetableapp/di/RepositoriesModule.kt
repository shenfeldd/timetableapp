package com.shenfeld.timetableapp.di

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.shenfeld.timetableapp.repositories.AppRepository
import com.shenfeld.timetableapp.repositories.AppStorage
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

val repositoriesModule: Module = module {
    single(named("TIMETABLE_PREFS")) { createSharedPreferences(context = androidContext(), prefName = "TIMETABLE_PREFS") }
    single { AppStorage(sharedPreferences = get(named("TIMETABLE_PREFS"))) }
    single { AppRepository(appStorage = get()) }
    single { createGsonInstance() }
}

private fun createSharedPreferences(context: Context, prefName: String): SharedPreferences {
    return context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
}

private fun createGsonInstance(): Gson {
    return GsonBuilder().create()
}