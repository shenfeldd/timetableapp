package com.shenfeld.timetableapp.di

import com.shenfeld.timetableapp.ui.discipline.DisciplineViewModel
import com.shenfeld.timetableapp.ui.home.HomeViewModel
import com.shenfeld.timetableapp.ui.pack_bag.PackBagViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelsModule: Module = module {
    viewModel { HomeViewModel(appRepository = get()) }
    viewModel { DisciplineViewModel(appRepository = get()) }
    viewModel { PackBagViewModel(appRepository = get()) }
}