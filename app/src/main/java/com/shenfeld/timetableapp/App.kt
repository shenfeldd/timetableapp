package com.shenfeld.timetableapp

import android.app.Application
import com.shenfeld.timetableapp.di.repositoriesModule
import com.shenfeld.timetableapp.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@App)
            modules(
                repositoriesModule,
                viewModelsModule,
            )
        }
    }
}