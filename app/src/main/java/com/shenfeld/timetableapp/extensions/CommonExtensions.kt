package com.shenfeld.timetableapp.extensions

import android.app.Application
import androidx.annotation.RawRes
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.gson.Gson
import org.koin.core.context.GlobalContext

inline fun <reified T> fromJsonToObject(@RawRes idRes: Int): T? {
    return try {
        val gson: Gson = GlobalContext.get().get()
        val app: Application = GlobalContext.get().get()
        val json = app.resources.openRawResource(idRes).use { it.bufferedReader().readText() }
        return gson.fromJson(json, T::class.java)
    } catch (e: Exception) {
        null
    }
}

inline fun <reified T> fromJsonToObject(json: String?): T? {
    return try {
        if (json.isNullOrEmpty()) return null
        val gson: Gson = GlobalContext.get().get()
        return gson.fromJson(json, T::class.java)
    } catch (e: Exception) {
        null
    }
}

inline fun <reified T> fromObjectToJson(value: T?): String {
    return try {
        if (value == null) return ""
        val gson: Gson = GlobalContext.get().get()
        return gson.toJson(value)
    } catch (e: Exception) {
        ""
    }
}

fun <T> T.asEvent(): Event<T> {
    return Event(this)
}

inline fun <T> LiveData<Event<T>>.observeEvent(
    owner: LifecycleOwner,
    crossinline observer: (value: T) -> Unit
) {
    this.observe(owner, Observer {
        it.getOnce { value ->
            observer.invoke(value)
        }
    })
}

inline fun <T> Event<T>.getOnce(block: (T) -> Unit) {
    block(getValue() ?: return)
}