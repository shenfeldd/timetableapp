package com.shenfeld.timetableapp.extensions

import android.content.SharedPreferences
import org.koin.core.component.KoinComponent
import kotlin.reflect.KProperty

class SharedPreferencesDelegate<T : Any>(
    private val sharedPreferences: SharedPreferences,
    private val key: String,
    private val defaultValue: T
) : KoinComponent {


    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return sharedPreferences.readValue(key, defaultValue)
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        sharedPreferences.saveValue(key, value)
    }

}