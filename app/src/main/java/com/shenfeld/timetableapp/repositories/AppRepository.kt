package com.shenfeld.timetableapp.repositories

import com.shenfeld.timetableapp.enums.WeekDay
import com.shenfeld.timetableapp.extensions.fromJsonToObject
import com.shenfeld.timetableapp.extensions.fromObjectToJson
import com.shenfeld.timetableapp.models.CheckBoxState
import com.shenfeld.timetableapp.models.DayItem
import com.shenfeld.timetableapp.models.Discipline
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AppRepository(private val appStorage: AppStorage) {

    private val defaultHashMap = hashMapOf(
        WeekDay.MONDAY.title to DayItem(
            weekDay = WeekDay.MONDAY,
            listDisciplines = listOf(Discipline.UNKNOWN),
            checkBoxesState = CheckBoxState(),
        ),
        WeekDay.TUESDAY.title to DayItem(
            weekDay = WeekDay.TUESDAY,
            listDisciplines = listOf(Discipline.UNKNOWN),
            checkBoxesState = CheckBoxState(),
        ),
        WeekDay.WEDNESDAY.title to DayItem(
            weekDay = WeekDay.WEDNESDAY,
            listDisciplines = listOf(Discipline.UNKNOWN),
            checkBoxesState = CheckBoxState(),
        ),
        WeekDay.THURSDAY.title to DayItem(
            weekDay = WeekDay.THURSDAY,
            listDisciplines = listOf(Discipline.UNKNOWN),
            checkBoxesState = CheckBoxState(),
        ),
        WeekDay.FRIDAY.title to DayItem(
            weekDay = WeekDay.FRIDAY,
            listDisciplines = listOf(Discipline.UNKNOWN),
            checkBoxesState = CheckBoxState(),
        ),
        WeekDay.SATURDAY.title to DayItem(
            weekDay = WeekDay.SATURDAY,
            listDisciplines = listOf(Discipline.UNKNOWN),
            checkBoxesState = CheckBoxState(),
        )
    )

    suspend fun getRemindersMap(): HashMap<String, DayItem> = withContext(Dispatchers.IO) {
        fromJsonToObject<Container>(appStorage.daysInfoMap)?.info ?: defaultHashMap
    }

    suspend fun set(name: WeekDay, newDayItem: DayItem) = withContext(Dispatchers.IO) {
        val map = getRemindersMap()
        map[name.title] = newDayItem
        setRemindersMap(map)
    }

    private fun setRemindersMap(hashMap: HashMap<String, DayItem>) {
        appStorage.daysInfoMap = fromObjectToJson(Container(info = hashMap))
    }

    suspend fun get(name: WeekDay): DayItem? = getRemindersMap()[name.title]

    data class Container(
        val info: HashMap<String, DayItem>
    )
}