package com.shenfeld.timetableapp.repositories

import android.content.SharedPreferences
import com.shenfeld.timetableapp.extensions.SharedPreferencesDelegate

class AppStorage(private val sharedPreferences: SharedPreferences) {

    companion object {
        private const val DAYS_STATE_KEY = "AppStorage.days_state_key"
    }


    var daysInfoMap: String by SharedPreferencesDelegate(sharedPreferences, DAYS_STATE_KEY, "{}")

}