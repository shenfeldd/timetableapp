package com.shenfeld.timetableapp.base.interfaces

interface BackButtonListener {

    fun onBackPressed(): Boolean

}