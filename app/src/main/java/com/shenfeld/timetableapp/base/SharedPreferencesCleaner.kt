package com.shenfeld.timetableapp.base

import android.content.SharedPreferences

class SharedPreferencesCleaner(
    private val defaultSharedPreferences: SharedPreferences,
) {
    fun clearDefaultSP() {
        defaultSharedPreferences.edit().clear().apply()
    }
}